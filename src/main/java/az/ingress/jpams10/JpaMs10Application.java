package az.ingress.jpams10;

import az.ingress.jpams10.domain.Student;
import az.ingress.jpams10.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class JpaMs10Application implements CommandLineRunner {

    private final StudentRepository studentRepository;

    public static void main(String[] args) {
        SpringApplication.run(JpaMs10Application.class, args);

    }

    @Override
    public void run(String... args) throws Exception {
        Student rasul = Student
                .builder()
                .age(20)
                .firstName("Rasul")
                .lastName("Musayev")
                .studentNumber("234")
                .build();

        Student orkhan = Student
                .builder()
                .age(20)
                .firstName("Orkhan")
                .lastName("Safarov")
                .studentNumber("133")
                .build();

        studentRepository.save(orkhan);
        studentRepository.save(rasul);

    }
}

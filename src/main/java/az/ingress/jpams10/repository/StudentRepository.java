package az.ingress.jpams10.repository;

import az.ingress.jpams10.domain.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface StudentRepository extends JpaRepository<Student,Long> {
    List<Student> findByFirstName(String orkhan); //select * from student where first_name='Orkhan'

    List<Student> findByFirstNameAndLastName(String orkhan, String safarov);

//    @Query("SELECT s FROM Student s WHERE s.firstName = ?1 AND s.lastName= ?2")
//    List<Student> findStudentsWithJpql(String firstName, String lastName);

    @Query(nativeQuery = true, value = "SELECT * FROM our_students s WHERE s.first_name = ?1 AND s.last_name= ?2")
    List<Student> findStudentsWithNativeSql(String orkhan, String safarov);

}




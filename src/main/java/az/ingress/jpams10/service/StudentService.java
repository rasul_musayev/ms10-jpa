package az.ingress.jpams10.service;


import az.ingress.jpams10.domain.Student;
import az.ingress.jpams10.domain.Student$;
import az.ingress.jpams10.repository.StudentRepository;
import com.speedment.jpastreamer.application.JPAStreamer;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
@RequiredArgsConstructor
public class StudentService {

    private final JPAStreamer jpaStreamer;
    private final StudentRepository studentRepository;

    //https://docs.spring.io/spring-data/jpa/docs/current/reference/html/
    //@PostConstruct
    public void listByQueryMethods() {
        studentRepository.findByFirstName("Orkhan")
                .stream()
                .forEach(System.out::println);

        studentRepository.findByFirstNameAndLastName("Orkhan", "Safarov")
                .stream()
                .forEach(System.out::println);

        studentRepository.findByFirstNameAndLastName(null, "Safarov")
                .stream()
                .forEach(System.out::println);
    }

    //@PostConstruct
//    public void jpql() {
//        studentRepository.findStudentsWithJpql("Orkhan", "Safarov")
//                .stream()
//                .forEach(System.out::println);
//    }

    //@PostConstruct
    public void nativeSql() {
        studentRepository.findStudentsWithNativeSql("Orkhan", "Safarov")
                .stream()
                .forEach(System.out::println);
    }

    //@PostConstruct
//    public void jpaStreamer() {
//        jpaStreamer.stream(Student.class) // Film.class is the @Entity representing the film-table
//                .filter(Student$.firstName.equal("Orkhan"))
//                .limit(5)
//                .forEach(System.out::println);
//    }
}